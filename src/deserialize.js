import { jsx } from 'slate-hyperscript'

export const deserialize = (el, markAttributes = {}) => {
  if (el.nodeType === Node.TEXT_NODE) {
    return jsx('text', markAttributes, el.textContent)
  } else if (el.nodeType !== Node.ELEMENT_NODE) {
    return null
  }
  const nodeAttributes = { ...markAttributes }

  switch (el.nodeName) {
    case 'STRONG':
      nodeAttributes.bold = true
      break
    case 'I':
      nodeAttributes.italic = true
      break
    case 'U':
      nodeAttributes.underline = true
      break
    case 'CODE':
      nodeAttributes.code = true
      break
    default:
      break
  }
  const children = Array.from(el.childNodes)
    .map((node) => deserialize(node, nodeAttributes))
    .flat()
  if (children.length === 0) {
    children.push(jsx('text', nodeAttributes, ''))
  }
  switch (el.nodeName) {
    case 'BODY':
      return jsx('fragment', {}, children)
    case 'BR':
      return '\n'
    case 'BLOCKQUOTE':
      return jsx('element', { type: 'quote' }, children)
    case 'P':
      return jsx('element', { type: 'paragraph' }, children)
    case 'A':
      return jsx('element', { type: 'link', url: el.getAttribute('href') }, children)
    default:
      return children
  }
}
