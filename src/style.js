export const style = () => ({
  container: {
    position: 'relative',
    background: 'white',
    borderRadius: '5px',
    border: '1px solid rgb(180, 202, 228)',
  },
  editor: {
    display: 'flex',
    flexDirection: 'row',
    '& > :first-child': {
      padding: '10px',
      width: '100%',
      boxSizing: 'border-box',
    },
  },
})
