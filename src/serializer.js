import escapeHtml from 'escape-html'
import { Text } from 'slate'

const serializeNode = (node) => {
  if (Text.isText(node)) {
    let string = escapeHtml(node.text)
    if (node.bold) {
      string = `<strong>${string}</strong>`
    }
    if (node.italic) {
      string = `<i>${string}</i>`
    }
    if (node.underline) {
      string = `<u>${string}</u>`
    }
    if (node.code) {
      string = `<code style={{ fontSize: '16px' }}>${string}</code>`
    }
    return string
  }

  const children = node.children.map((n) => serializeNode(n)).join('')

  switch (node.type) {
    case 'quote':
      return `<blockquote><p>${children}</p></blockquote>`
    case 'paragraph':
      return `<p>${children}</p>`
    case 'link':
      return `<a href="${escapeHtml(node.url)}">${children}</a>`
    default:
      return children
  }
}

export const serialize = (value) => {
  let children = ''

  for (const node of value) {
    children += serializeNode(node)
  }

  return children
}
