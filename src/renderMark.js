import React from 'react'

export const renderMark = (props) => {
  const { children, leaf, attributes } = props
  let mark = children

  if (leaf.bold) {
    mark = <strong>{mark}</strong>
  }
  if (leaf.italic) {
    mark = <em>{mark}</em>
  }
  if (leaf.underline) {
    mark = <u>{mark}</u>
  }
  if (leaf.code) {
    mark = <code style={{ fontSize: '16px' }}>{mark}</code>
  }

  return <span {...attributes}>{mark}</span>
}
