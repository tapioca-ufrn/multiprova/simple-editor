export const style = () => ({
  root: {
    minWidth: 0,
    minHeight: 0,
    padding: '2px 5px',
    color: 'rgba(0, 0, 0, 0.45)',
    '&:not(:last-child)': {
      marginRight: '2px',
    },
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
    },
  },
})
