import React from 'react'
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'

export const ButtonComponent = (props) => {
  const { classes, tooltip, Icon, onClick, editor } = props

  return (
    <Tooltip title={tooltip ? tooltip : ''}>
      <Button classes={{ root: classes.root }} onClick={() => onClick(editor)}>
        <Icon />
      </Button>
    </Tooltip>
  )
}
