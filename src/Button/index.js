import { withStyles } from '@material-ui/core/styles'

import { ButtonComponent } from './ButtonComponent'
import { style } from './style'

export const Button = withStyles(style)(ButtonComponent)
