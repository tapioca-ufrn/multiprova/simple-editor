import { Editor, Text, Transforms } from 'slate'

export const CustomEditor = {
  isBoldMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: (n) => n.bold === true,
      universal: true,
    })

    return !!match
  },

  toggleBoldMark(editor) {
    const isActive = CustomEditor.isBoldMarkActive(editor)
    Transforms.setNodes(editor, { bold: isActive ? null : true }, { match: (n) => Text.isText(n), split: true })
  },
  isItalicMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: (n) => n.italic === true,
      universal: true,
    })

    return !!match
  },
  toggleItalicMark(editor) {
    const isActive = CustomEditor.isItalicMarkActive(editor)
    Transforms.setNodes(editor, { italic: isActive ? null : true }, { match: (n) => Text.isText(n), split: true })
  },

  isUnderlinedMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: (n) => n.underline === true,
      universal: true,
    })

    return !!match
  },
  toggleUnderlinedMark(editor) {
    const isActive = CustomEditor.isUnderlinedMarkActive(editor)
    Transforms.setNodes(editor, { underline: isActive ? null : true }, { match: (n) => Text.isText(n), split: true })
  },
  isMonospaceMarkActive(editor) {
    const [match] = Editor.nodes(editor, {
      match: (n) => n.code === true,
      universal: true,
    })

    return !!match
  },
  toggleMonospacedMark(editor) {
    const isActive = CustomEditor.isMonospaceMarkActive(editor)
    Transforms.setNodes(editor, { code: isActive ? null : true }, { match: (n) => Text.isText(n), split: true })
  },
}
