import React from 'react'
import { Button } from '../Button'
import FormatBold from '@material-ui/icons/FormatBold'
import FormatItalic from '@material-ui/icons/FormatItalic'
import FormatUnderlined from '@material-ui/icons/FormatUnderlined'
import Code from '@material-ui/icons/Code'
import { CustomEditor } from './actions'

export const ToolbarComponent = (props) => {
  const { classes, editor } = props

  return (
    <div className={classes.toolbar}>
      <Button tooltip="Negrito" Icon={FormatBold} onClick={CustomEditor.toggleBoldMark} editor={editor} />
      <Button tooltip="Itálico" Icon={FormatItalic} onClick={CustomEditor.toggleItalicMark} editor={editor} />
      <Button
        tooltip="Sublinhado"
        Icon={FormatUnderlined}
        onClick={CustomEditor.toggleUnderlinedMark}
        editor={editor}
      />
      <Button tooltip="Monospace" Icon={Code} onClick={CustomEditor.toggleMonospacedMark} editor={editor} />
    </div>
  )
}
