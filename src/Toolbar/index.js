import { withStyles } from '@material-ui/core/styles'

import { ToolbarComponent } from './ToolbarComponent'
import { style } from './style'

export const Toolbar = withStyles(style)(ToolbarComponent)
