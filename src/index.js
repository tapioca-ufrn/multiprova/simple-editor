import { withStyles } from '@material-ui/core/styles'

import { SimpleEditorComponent } from './SimpleEditorComponent'
import { style } from './style'

export const SimpleEditor = withStyles(style)(SimpleEditorComponent)
