import React, { useState, useMemo } from 'react'
import { createEditor } from 'slate'
import { Slate, Editable, withReact } from 'slate-react'
import { Toolbar } from './Toolbar'
import { renderMark } from './renderMark'
import { renderElement } from './renderElement'
import { serialize } from './serializer'
import { deserialize } from './deserialize'

export const SimpleEditorComponent = (props) => {
  const { classes } = props

  const [editor] = useState(() => withReact(createEditor()))

  const onChange = (value) => {
    const isAstChange = editor.operations.some((op) => 'set_selection' !== op.type)
    if (isAstChange) {
      localStorage.setItem('content', serialize(value))
    }
  }

  const getInitialValue = () => {
    const content = localStorage.getItem('content')
    const document = new DOMParser().parseFromString(content, 'text/html')
    return deserialize(document.body)
  }

  const initialValue = useMemo(
    () =>
      getInitialValue() || [
        {
          type: 'paragraph',
          children: [{ text: '' }],
        },
      ],
    [],
  )

  return (
    <Slate editor={editor} value={initialValue} onChange={(value) => onChange(value)}>
      <div className={classes.container}>
        <Toolbar editor={editor} />
        <div className={classes.editor}>
          <Editable renderElement={renderElement} renderLeaf={renderMark} />
        </div>
      </div>
    </Slate>
  )
}
