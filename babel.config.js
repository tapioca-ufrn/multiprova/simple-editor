// Babel é necessário devido a regras do linter que dependem dele.
module.exports = {
  presets: [
    '@babel/preset-react',
    [
      '@babel/preset-env',
      {
        shippedProposals: true,
      },
    ],
  ],
  plugins: ['@babel/plugin-proposal-class-properties'],
  env: {
    test: {
      plugins: ['@babel/plugin-transform-runtime'],
    },
  },
  include: ['src/**'],
}
